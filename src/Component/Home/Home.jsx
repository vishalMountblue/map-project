import React, { useEffect, useState } from "react";
import { Box, Button, Container, TextField, Typography } from "@mui/material";
import Map from "../Map/Map";
import { useForm } from "react-hook-form";
import { fetchAddresses } from "../../api";
import Addresses from "../Addresses/Addresses";
import CircularProgress from "@mui/material/CircularProgress";
import Alert from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";
import RecentSearch from "../RecentSearch/RecentSearch";

const Home = () => {
  let style = {
    height: "100vh",
    minHeight: "100vh",
    width: "100vw",
    display: "flex",
    backgroundColor: "#F8F6F4",
    overflow: "auto",
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [features, setFeatures] = useState([]);
  const [position, setPosition] = useState([42.361145, -71.057083]);
  const [population, setPopulation] = useState({});
  const [recentPlacesArrayLength, setRecentPlacesArrayLength] = useState();
  const [placeName, setPlaceName] = useState();
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const ADMINISTRATIVE = "administrative";

  let recentSearches = JSON.parse(localStorage.getItem("recentPlaces")) || [];

  useEffect(() => {
    setRecentPlacesArrayLength(recentSearches.length);
  }, []);

  const validateInputs = (data) => {
    let placeName = data.place;

    if (!recentSearches.includes(placeName)) {
      recentSearches.push(placeName);
      localStorage.setItem("recentPlaces", JSON.stringify(recentSearches));
      setRecentPlacesArrayLength(recentSearches.length);
    }

    getAllAddresses(placeName);
  };

  const getAllAddresses = async (placeName) => {
    setLoading(true);
    let response = await fetchAddresses(placeName);

    if (response.error) {
      setLoading(false);
      setIsError(response.error);
    } else {
      setLoading(false);
      setFeatures(response.data);
    }
  };

  const getSelectedAddress = (address) => {
    let selectedAddressObject = address;

    console.log("selectedAddressObject", selectedAddressObject);

    let latitude = Number(selectedAddressObject.lat);
    let longitude = Number(selectedAddressObject.lon);
    let population = selectedAddressObject.extratags.population;
    let populationYear = selectedAddressObject.extratags["population:date"];
    let coordinates = [latitude, longitude];

    setPosition(coordinates);
    setPopulation({ population, populationYear });
  };

  const clearAllRecentSearch = () => {
    localStorage.setItem("recentPlaces", JSON.stringify([]));
    setRecentPlacesArrayLength(0);
  };

  const getRecentSearchText = (recentSearch) => {
    console.log("recent search", recentSearch);
    let placeField = register ("place")
    placeField.value = recentSearch
    getAllAddresses(recentSearch);
  };

  const hideSnackbar = () => {
    setIsError(false);
  };

  return (
    <>
      <Box sx={style}>
        <Box
          sx={{
            width: "30%",
            minHeight: "100vh",
            height: "fit-content",
            background: "linear-gradient(to right, #A1C2F1, #5A96E3)",
            m: 0,
          }}
        >
          <Container sx={{ my: 2, overflow: "auto" }}>
            <form onSubmit={handleSubmit(validateInputs)}>
              <TextField
                {...register("place", {
                  required: true,
                  minLength: {
                    value: 2,
                    message: "Please type appropriate place name!",
                  },
                })}
                sx={{ backgroundColor: "white", borderRadius: "5px" }}
                placeholder="Place"
                fullWidth
              ></TextField>
              {errors.place?.type === "required" && (
                <p role="alert" style={{ color: "#CD1818" }}>
                  Place name is required
                </p>
              )}

              <Button variant="contained" type="submit" sx={{ my: 2 }}>
                Search
              </Button>
            </form>

            {loading ? (
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <CircularProgress sx={{ color: "white" }} />
              </Box>
            ) : (
              <Box sx={{ height: "100%" }}>
                <Box sx={{ minHeight: "500px", overflow: "auto" }}>
                  {features.map((feature) => {
                    if (feature.type === ADMINISTRATIVE) {
                      return (
                        <Addresses
                          optionData={feature}
                          selectedCard={getSelectedAddress}
                          key={feature.place_id}
                        />
                      );
                    }
                  })}
                </Box>

                <Box sx={{ Height: "500px", overflow: "auto" }}>
                  <Box
                    sx={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <Typography variant="h5" sx={{ color: "white" }}>
                      Recent Searches
                    </Typography>
                    <Button variant="contained" onClick={clearAllRecentSearch}>
                      Clear All
                    </Button>
                  </Box>

                  {recentSearches.map((searchName) => {
                    return (
                      <RecentSearch
                        placeName={searchName}
                        recentSearch={getRecentSearchText}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}
          </Container>
        </Box>

        <Box
          sx={{
            width: "70%",
            height: "100%",
            p: 0,
            m: 0,
          }}
        >
          <Container sx={{ height: "100%", width: "100%", m: 0, p: 0 }}>
            <Map position={position} population={population} />
          </Container>

          <Container sx={{ my: 1 }}>
            {isError && (
              <Snackbar
                open={isError}
                autoHideDuration={3000}
                onClose={hideSnackbar}
              >
                <Alert severity="error">{isError}</Alert>
              </Snackbar>
            )}
          </Container>
        </Box>
      </Box>
    </>
  );
};

export default Home;
