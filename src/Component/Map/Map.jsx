import React, { useRef } from "react";
import { MapContainer, TileLayer, Popup, Marker } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { Typography } from "@mui/material";

const Map = (props) => {
  const position = props.position;
  const map = useRef();

  if (map.current) {
    map.current.flyTo(position, 13);
  }
  return (
    <>
      <MapContainer
        center={position}
        zoom={13}
        scrollWheelZoom={true}
        ref={map}
        style={{ width: "100%", height: "100%" }}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position} >
          <Popup>
            {props.population.population ? (
              <Typography>
                {" "}
                population: {props.population.population}
              </Typography>
            ):"Population: N/A" }

            <br/>

            {props.population.populationYear ? (
              <Typography>
                {" "}
                population Year: {props.population.populationYear}
              </Typography>
            ):"Population year: N/A"}
          </Popup>
        </Marker>
      </MapContainer>
    </>
  );
};

export default Map;
