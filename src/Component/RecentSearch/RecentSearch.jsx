import React from "react";
import { Box, Card } from "@mui/material";
import GpsFixedIcon from "@mui/icons-material/GpsFixed";

const RecentSearch = (props) => {
  let searchText = props.placeName;
  const handleRecentSearchClick = () =>{
    props.recentSearch(searchText)
  }
  return (
    <>
      <Card sx={{ my: 2, p: 2, cursor: "pointer" }} style={{ color: "black" }} onClick={handleRecentSearchClick}>
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <GpsFixedIcon sx={{ mx: 2 }} />
          {searchText}
        </Box>
      </Card>
    </>
  );
};

export default RecentSearch;
