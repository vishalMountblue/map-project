import { Box, Card } from "@mui/material";
import GpsFixedIcon from "@mui/icons-material/GpsFixed";
import React from "react";

const Addresses = (props) => {
  let address = props.optionData;

  const handleSelectedCard = () => {
    props.selectedCard(address);
  };
  return (
    <>
      <Card
        sx={{ my: 2, p: 2, cursor: "pointer" }}
        style={{ color: "black" }}
        onClick={handleSelectedCard}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <GpsFixedIcon sx={{ mx: 2 }} />
          {address.display_name}
        </Box>
      </Card>
    </>
  );
};

export default Addresses;
