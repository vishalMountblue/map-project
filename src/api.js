import axios from "axios";
export const fetchAddresses = async (place) => {
  try {
    let placeResponse = await axios.get(
      `https://nominatim.openstreetmap.org/search?extratags=1&format=json&q=${place}&limit=10`
    );
    return placeResponse;
  } catch (err) {
    return { error: err.message };
  }
};
