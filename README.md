# Location Search

This project is a location Search web application, where you can search any place and get addresses particular to that location and on clicking any location you will be redirected to that particular location on map.
This project uses [Nominatim](https://nominatim.org/release-docs/latest/api/Search/#parameters) api to fetch the required data.

## Installation

Install all the required dependencies using

```
  npm install

```

## Run Locally

use this command to run the project

```
  npm run dev

```

## Project flow
![project flow](public/projectFlow.png)

## Approach

- There is search option for place, on clicking search button addresses to that particular location are displayed.

- I have used __useForm()__ to take the input and show error if place is left empty.

- I hve initialized __features__ variable as __useState()__ and i am storing all addresses in that feature variable, so when a new place is searched features gets assigned with new addresses and addresses cards are re rendered.

- I have initialized __position__ variable as __useState()__ and default position is latitude and longitude of Boston, when user clicks on any address the position is updated with respected latitude and longitude resulting in movement of marker in map to that particular location.

- __population__ is also initialized with __useState()__, because population changes when user clicks on any particular address.

- __position__ and __population__ are passed as prop to __Map__ component and __feature__ is passed as prop to __Address__ component.

## Technologies used

- Vite is a build tool and development server for modern web applications.
- React - React is a JavaScript library for building user interfaces. It is widely used for developing interactive and dynamic web applications.
- HTML and CSS - used for designing the webpage
- JavaScript
- Material UI

### React Hooks in this project

- useState
- useRef
- useForm

## References

- [Nominatim](https://nominatim.org/release-docs/latest/api/Search/#parameters)
- [Material ui](https://mui.com/material-ui/getting-started/overview/)
- [React](https://react.dev/reference/react)
